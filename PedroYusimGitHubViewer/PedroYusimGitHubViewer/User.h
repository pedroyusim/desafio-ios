//
//  User.h
//  PedroYusimGitHubViewer
//
//  Copyright © 2016 Pedro Yusim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property NSString *login;
@property NSString *avatarUrl;

@end
