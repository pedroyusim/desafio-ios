//
//  PullRequestsViewController.h
//  PedroYusimGitHubViewer
//
//  Copyright © 2016 Pedro Yusim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepositorySelectionDelegate.h"
#import "Repository.h"

@interface PullRequestsViewController : UIViewController <RepositorySelectionDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UILabel *labelFirstSelection;

@property (strong, nonatomic) IBOutlet UITableView *tableViewPullRequests;

@end
