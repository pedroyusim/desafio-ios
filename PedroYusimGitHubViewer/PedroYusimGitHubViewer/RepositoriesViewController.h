//
//  RepositoriesViewController.h
//  PedroYusimGitHubViewer
//
//  Copyright © 2016 Pedro Yusim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GitHubAPIRepository.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "RepositoryCell.h"
#import "RepositorySelectionDelegate.h"

@interface RepositoriesViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableViewRepos;
@property (nonatomic, assign) id<RepositorySelectionDelegate> delegate;

@end
