//
//  RepositorySelectionDelegate.h
//  PedroYusimGitHubViewer
//
//  Copyright © 2016 Pedro Yusim. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Repository;
@protocol RepositorySelectionDelegate <NSObject>

@required
-(void)selectedRepository:(Repository *)repo;

@end
