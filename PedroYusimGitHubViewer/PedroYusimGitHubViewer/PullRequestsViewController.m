//
//  PullRequestsViewController.m
//  PedroYusimGitHubViewer
//
//  Copyright © 2016 Pedro Yusim. All rights reserved.
//

#import "PullRequestsViewController.h"
#import "GitHubAPIRepository.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "PullRequestCell.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface PullRequestsViewController ()

@property NSArray *arrayPullRequestsToShow;

@end

@implementation PullRequestsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"PullRequestsViewController viewDidLoad");
    
    [self.navigationItem setTitle:@"Pull Requests"];
    
    self.arrayPullRequestsToShow = [NSArray array];
    
    [self.tableViewPullRequests setTableFooterView:[[UIView alloc] init]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.arrayPullRequestsToShow != nil && [self.arrayPullRequestsToShow count] > 0) {
        return [self.arrayPullRequestsToShow count];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.arrayPullRequestsToShow != nil && [self.arrayPullRequestsToShow count] > 0) {
        PullRequestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pullRequestCell"];
        
        PullRequest *pullReq = [self.arrayPullRequestsToShow objectAtIndex:indexPath.row];
        
        cell.labelPullRequestTitle.text = pullReq.title;
        cell.labelPullRequestDescription.text = pullReq.body;
        
        cell.labelOwnerUsername.text = pullReq.user.login;
        //Objeto retornado por API não tem este valor, portanto, setamos para string vazia.
        cell.labelOwnerFullName.text = @"";
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        
        NSDate *createdDate = [dateFormatter dateFromString:pullReq.createdAt];
        
        NSDateFormatter *commomDateFormatter = [[NSDateFormatter alloc] init];
        [commomDateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
        
        cell.labelPullRequestCreationTime.text = [commomDateFormatter stringFromDate:createdDate];
        
        [cell.imageViewOwnerAvatar sd_setImageWithURL:[NSURL URLWithString:pullReq.user.avatarUrl] placeholderImage:[UIImage imageNamed:@"owner-placeholder"]];
        
        return cell;
    }
    
    
    return [[UITableViewCell alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PullRequest *pullReq = [self.arrayPullRequestsToShow objectAtIndex:indexPath.row];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:pullReq.htmlUrl]];
}


#pragma mark - CustomDelegate Methods

- (void)selectedRepository:(Repository *)repo {
    NSLog(@"Carregar pull requests");
    
    [self.navigationItem setTitle:repo.name];
    
    self.arrayPullRequestsToShow = [NSArray array];
    [self.tableViewPullRequests reloadData];
    
    [self callGetPullRequests:repo];
}

#pragma mark - Webservice Methods

- (void)callGetPullRequests:(Repository *)repo {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            repo.name, @"repository",
                            repo.owner.login, @"owner",
                            nil];
    
    [[GitHubAPIRepository sharedRepository] callGetPullRequests:params success:^(NSArray *respObj) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"success!!!");
        
        if(respObj != nil) {
            if([respObj count] > 0) {
                [self.tableViewPullRequests setHidden:NO];
                [self.labelFirstSelection setHidden:YES];
                
                self.arrayPullRequestsToShow = respObj;
                
                [self.tableViewPullRequests reloadData];
            } else {
                [self.tableViewPullRequests setHidden:YES];
                [self.labelFirstSelection setHidden:NO];
                self.labelFirstSelection.text = @"Este repositório não contém nenhum Pull Request aberto.";                
            }
        } else {
            [self.tableViewPullRequests setHidden:YES];
            [self.labelFirstSelection setHidden:NO];
            
            self.labelFirstSelection.text = @"Ocorreu algum erro ao carregar a lista de Pull Requests. Por favor, tente novamente.";
        }
        
    } error:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        NSLog(@"failure!!!");
        
        [self.tableViewPullRequests setHidden:YES];
        [self.labelFirstSelection setHidden:NO];
        
        self.labelFirstSelection.text = @"Ocorreu algum erro ao carregar a lista de Pull Requests. Por favor, tente novamente.";
    }];
}

@end
