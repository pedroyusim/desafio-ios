//
//  RepositoriesViewController.m
//  PedroYusimGitHubViewer
//
//  Copyright © 2016 Pedro Yusim. All rights reserved.
//

#import "RepositoriesViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PullRequestsViewController.h"

@interface RepositoriesViewController ()

@property NSMutableArray *arrayRepositoriesToShow;

@property int currentPage;

@property BOOL shouldKeepReloadingPages;

@end

@implementation RepositoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.arrayRepositoriesToShow = [NSMutableArray array];
    self.currentPage = 1;
    
    self.shouldKeepReloadingPages = YES;
    
    [self callJavaRepos];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - TableView Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.arrayRepositoriesToShow != nil && [self.arrayRepositoriesToShow count] > 0) {
        return [self.arrayRepositoriesToShow count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.arrayRepositoriesToShow != nil && [self.arrayRepositoriesToShow count] > 0) {
        RepositoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"repoCell"];
        
        Repository *currentRepo = [self.arrayRepositoriesToShow objectAtIndex:indexPath.row];
        
        cell.labelRepoName.text = currentRepo.name;
        cell.labelRepoDescription.text = currentRepo.repositoryDescription;
        cell.labelForkCount.text = [NSString stringWithFormat:@"%@", currentRepo.forksCount];
        cell.labelStarsCount.text = [NSString stringWithFormat:@"%@", currentRepo.stargazersCount];
        
        cell.labelOwnerUsername.text = currentRepo.owner.login;
        
        [cell.imageViewOwnerAvatar sd_setImageWithURL:[NSURL URLWithString:currentRepo.owner.avatarUrl] placeholderImage:[UIImage imageNamed:@"owner-placeholder"]];
        
        //Não há onde pegar esta informação, portanto, não exibo.
        cell.labelOwnerFullName.text = @"";
        
        if(indexPath.row == [self.arrayRepositoriesToShow count] - 1 && self.shouldKeepReloadingPages) {
            self.currentPage++;
            [self callJavaRepos];
        }
        
        return cell;
    }
    
    return [[UITableViewCell alloc] init];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //Chamar Controller de PullRequests
    Repository *selectedRepo = [self.arrayRepositoriesToShow objectAtIndex:indexPath.row];
    if(self.delegate) {
        [self.delegate selectedRepository:selectedRepo];
        
        PullRequestsViewController *pullReqsController = (PullRequestsViewController *)self.delegate;
        
        [self.splitViewController showDetailViewController:pullReqsController.navigationController sender:nil];
    }
    
}


#pragma mark - API Methods

- (void)callJavaRepos {
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:self.currentPage], @"page", nil];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSLog(@"CurrentPage: %d", self.currentPage);

    [[GitHubAPIRepository sharedRepository] callSearchJavaRepos:params success:^(NSArray *respObj) {
        NSLog(@"SUCCESS!!");
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if(respObj != nil && [respObj count] > 0) {
            [self.arrayRepositoriesToShow addObjectsFromArray:respObj];
            
            [self.tableViewRepos reloadData];
        } else {
            self.shouldKeepReloadingPages = NO;
        }
        
    } error:^(NSError *error) {
        NSLog(@"ERROR!!!");
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        self.shouldKeepReloadingPages = NO;
    }];
}


@end
